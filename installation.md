# Installation Overview
Follow the instructions below to quickly and easily install Mango.

General Installation steps:

1. Download and install the latest Java **JDK** (see [About Java](#about-java) below).
2. Download the Mango full zip file [here](https://store.infiniteautomation.com/core).
3. Unzip Mango into a directory of your choice.
4. Start Mango with one of the start-up scripts in the `/bin` folder.

## Running on Windows
Installing Mango Automation on a Windows computer is very easy. Follow these steps.

1. Open your start menu and search for `cmd`.
2. Right Click on `cmd.exe` and select **Run As Administrator**, this will open the Command Prompt.
3. Type `cd C:\<your mango directory>/bin` such as `cd C:\Program/ Files\Mango\bin`.
4. Type `ma-start.bat` to start Mango.

### Troubleshooting
After clicking on the `/bin/ma-start.bat` file if you see anything or something popped up and closed quickly then there was an error.

To see the error, first open the Command Prompt by typing `cmd` into your start menu. Right click on it and select **Run as Administrator**

Change directories to your `Mango\bin` folder. Now you can type `ma-start.bat` and watch the start up procedure. If there is a conflict with your system you will see the error in the terminal.

Common Startup issues:

1. **JDK** not installed.
2. **JAVA_HOME** Not Set correctly.
3. Something else occupying the port Mango is starting on. By default Mango uses port **8080**. You can change this in the `/classes/env.properties` file.

[![Windows Installation](./img/windows-installation-video.png)](https://vimeo.com/115147084)

## Installing on Linux
Linux is our prefered operating system to run Mango on in production enviroments.  Although Mango runs fine on Windows and Mac Linux is very stable, offers excellent performance and can be much easier to manage the system.

It is however only recomened to use Linux as your Mango operating system if you are already familiar and confortable with administering Linux systems.  For this reason these installation instructions assume knowledge of the Linux systems already exists.

You can check to see if you have the **JDK** installed with:

```bash
java -version
```

and

```bash
echo $JAVA_HOME
```
Installing mango to your system is easy. This is done by adding a directory to your system called mango

```bash
mkdir /opt/mango
```

Now download mango from Infinite Automation site, then move it to the newly created directory and unzip it (you may need to install the linux package `unzip` first).

Before you init Mango make sure have given exectution permissions to all `.sh` files inside `./bin` folder.

```bash
sudo chmod +x *.sh 
```

Start **Mango** with the start up script in the bin folder in your mango directory 

```bash
sudo ./bin/ma.sh start
```

Also see the wiki to install the [Linux Service](#linux-service) so Mango start automatically on bootup.

## Installing on Mac
For Mac OS, you can follow this video tutorial:

[![Mac Installation](./img/mac-installation-video.png)](https://vimeo.com/115149086)

## Installing Services
These services are used to start Mango automatically on bootup.

### Linux Service
These instructions are for Debian and Linux-based systems. Installing a service for Linux can be very useful if you want to start, stop, or even restart mango.

With that said, a full Linux service tutorial is out of the reach of this wiki. But please feel free to look at these links:

* https://wiki.debian.org/LSBInitScripts
* https://wiki.debian.org/Daemon
* http://debian-handbook.info/browse/stable/unix-services.html


#### Service Install Instructions
First, go to `init.d` folder and create a file named `mango`

```bash
cd /etc/init.d
sudo nano mango
```

Add this code to the file

```bash
#!/bin/sh
# chkconfig: 235 99 10
# description: Start or stop the Mango Automation
#
### BEGIN INIT INFO
# Provides: mango
# Required-Start: $network $syslog
# Required-Stop: $network
# Default-Start: 2 3 5
# Default-Stop: 0 1 6
# Description: Start or stop the Mango Automation
### END INIT INFO
start="/opt/mango/bin/ma.sh start"
stop="/opt/mango/bin/ma.sh stop"
lockfile=/var/lock/subsys/mango
name='Mango'
case "$1" in
'start')
$start>/dev/null 2>&1 </dev/null
RETVAL=$?
if [ "$RETVAL" = "0" ]; then
touch $lockfile>/dev/null 2>&1
fi
;;
'stop')
$stop
RETVAL=$?
if [ "$RETVAL" = "0" ]; then
rm -f $lockfile
fi
;;
'status')
status Mango
;;
'restart'|'reload'|'condrestart')
$stop ; $start
RETVAL=$?
;;
*)
echo "Usage: sudo service{ start | stop | restart | reload | condrestart}"
RETVAL=1
;;
esac
exit $RETVAL
```

Please note that if you have mango installed at a location other than: `/opt/mango` you will need to change the start and stop variables to your full path in which mango is installed.

Press `ctrl+x` then save the file.

Next we need to set the permissions for the service to be executable.

```bash
sudo chmod u+x /etc/init.d/mango
```

Next we need to tell the system about this on Debian-based systems (Ubuntu, Linux Mint, etc).

```bash
sudo update-rc.d  mango defaults
```

That's it! You can now start and stop the services like so:

#### stop
```bash
sudo service mango stop
```

#### start
```bash
sudo service mango start
```

#### restart
```bash
sudo service mango restart
```

#### status
```bash
sudo service mango status
```

### Windows Service
We recommend using **YAJSW** for installing a windows service. We are providing a prepackaged file to make this very easy:

1. Download the zip file yajsw.zip Unzip the fine in your MA_HOME folder.
2. Inside the `MA_HOME/yajsw/conf/wrapper.conf` file you can edit the location where Mango is installed. By default it assumes Mango is installed at `C:\Mango`
3. In the command prompt cd to MA_HOME\yajsw\bat and they run installService.bat.
4. After the service is installed you can open up your windows services screen and start or stop the service named Mango.

!> **Note**: When running as a windows service in this way the automatic upgrade of the Mango core will not fully complete and you will have to finish the upgrade manually by deleting the `MA_HOME\lib` folder and unzipping the `m2m2-core.x.x.x.zip` file that will be downloaded during the upgrade process.

## About Java
We recommend Oracle Java. You can download the appropriate JDK from Oracle at this [link](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).

1. Select **JDK Download**
2. Read and accept the Oracle License Agreement for the latest version of the **Java SE Development Kit**
3. Select the appropriate file for the operating system on which you intend to install Mango

On some systems you may also need to set an environmental variable **JAVA_HOME** pointing to your JDK install directory. This is covered under the installation procedure for each operating system.

## Default Login Info

Mango installs with a default admin user. After logging in for the first time, the password should be changed.

* Username: **admin**
* Password: **admin**