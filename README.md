## Mango Automation

**Mango** a 100% browser-based, cross platform software application that enables users to access and control electronic sensors, PLC’s, devices, databases or web services over multiple protocols simultaneously. Mango provides an interface with which diverse data sources can be created and configured while providing downstream management of user access, alerts, data logging, alarms and automation.

Powerful data visualization tools bring your data to life with dynamic dashboards that allow you to better analyze and aggregate your data, allowing your users to make more informed decisions.

Mango Automation includes everything you need to build a powerful, scalable and modern systems.

## Features
- Built In Protocols.
- High Performance Time Series Historian.
- Built in Analytics.
- Reporting & Billing.
- Advanced Schedules with Exception Calendar.
- System Scalability.
- Leading Edge Data Visualization.
- Automation and Alarming.
- Powerful IIoT & Remote Site Monitoring.

## Community

Users and development team are in the [Forum](https://forum.infiniteautomation.com/).