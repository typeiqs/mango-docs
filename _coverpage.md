
![logo](_media/logoMango.svg)

> A 100% browser-based, cross platform software application. 

* Uses all standard HTML and includes a powerful toolbox to create stunning Dashboards, Web Apps and HMI Screens.

[Get Started](#mango-automation)
[Website](https://infiniteautomation.com)