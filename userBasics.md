# Mango Automation User Interface Overview

In the next video you'll get an overview of the new user interface and an introduction to many of the new features and tools.

[![Mango Overview](./img/mango-automation-overview-video.png)](https://vimeo.com/215444420)