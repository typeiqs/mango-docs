# Instalación
Sigue las instrucciones a continuación para instalar Mango de forma rápida y fácilmente.

Pasos generales de instalación:

1. Descarga e instala el último Java **JDK** (ver [Acerca de Java](#acerca-de-java) abajo).
2. Descarga el archivo zip de Mango [aquí](https://store.infiniteautomation.com/core).
3. Descomprime Mango en un directorio de tu elección.
4. Inicie Mango con uno de los scripts de inicio en la carpeta `/bin`.

## Iniciar en Windows
Instalar Mango Automation en una computadora con Windows es muy fácil. Sigue estos pasos.

1. Abre el menú de inicio y busca `cmd`.
2. Haga clic con el botón derecho en `cmd.exe` y selecciona **Ejecutar como administrador**, esto abrirá el Símbolo del sistema.
3. Escribe `cd C:\<Tu directorio de mango>\bin` como `cd C:\Archivos/ de/ programa\Mango\bin`
4. Escriba `ma-start.bat` para iniciar Mango

### Solución de problemas
Después de hacer clic en el archivo `/bin/ma-start.bat` si ve algo que apareció y cerró rápidamente, entonces hubo un error.

Para ver el error, primero abre el símbolo del sistema escribiendo `cmd` en su menú de inicio. Haga clic derecho sobre él y selecciona **Ejecutar como administrador**.

Navega a la carpeta `bin` de Mango. Ahora puede escribir `ma-start.bat` y observar el procedimiento de inicio. Si hay un conflicto con su sistema, verá el error en el terminal.


Problemas comunes de inicio:

1. El **JDK** no está instalado.
2. **JAVA_HOME** no está configurado correctamente.
3. Algo más ocupando el puerto de Mango está utilizando. De forma predeterminada, Mango usa el puerto **8080**. Puede cambiar esto en el archivo `/classes/env.properties`.

[![Windows Installation](../img/windows-installation-video.png)](https://vimeo.com/115147084)

## Instalación en Linux
Linux es nuestro sistema operativo preferido para ejecutar Mango en entornos de producción. Aunque Mango funciona bien en Windows y Mac, Linux es muy estable, ofrece un excelente rendimiento y puede ser mucho más fácil de administrar el sistema.

Sin embargo, solo se recomienda utilizar Linux como su sistema operativo Mango si ya está familiarizado y se siente cómodo con la administración de sistemas Linux. Por esta razón, estas instrucciones de instalación asumen que cuenta con el conocimiento necesario sobre Linux.

Puedes chequear que el **JDK** está instalado con:

```bash
java -version
```

y

```bash
echo $JAVA_HOME
```
Instalar mango en su sistema es sencillo. Debe crear una carpeta llamada `mango` en el directorio `/opt`.

```bash
mkdir /opt/mango
```

Ahora descargue el mango del sitio de Infinite Automation, luego muévalo al directorio recién creado y descomprímalo (puede que necesite instalar primero el paquete de Linux `unzip`).

Antes de iniciar Mango debe dar permisos de ejecución a todos los archivos `.sh` dentro de la carpeta `./bin`.

```bash
sudo chmod +x *.sh 
```

Inicia el **Mango** con el archivo de arranque dentro de la carpeta `./bin` 

```bash
sudo ./bin/ma.sh start
```

También vea la wiki para instalar el [Servicio de Linux](#linux-service) para que Mango se inicie automáticamente al arrancar.


## Instalación en Mac
Para el SO Mac, puedes seguir el siguiente tutorial:

[![Mac Installation](../img/mac-installation-video.png)](https://vimeo.com/115149086)

## Instalación de Servicios
Estos servicios son usados para iniciar **Mango** automáticamente en el arranque del SO.

### Servicios en Linux
Estas instrucciones son para sistemas basados en Debian y Linux

Instalar un servicio para Linux puede ser muy útil si desea iniciar, detener o incluso reiniciar el mango.

Dicho esto, un tutorial de servicio completo de Linux está fuera del alcance de esta wiki. Pero no dude en consultar estos enlaces:

* https://wiki.debian.org/LSBInitScripts
* https://wiki.debian.org/Daemo
* http://debian-handbook.info/browse/stable/unix-services.html

#### Instalación
Primero, dirígete a la carpeta `init.d` y crea un archivo llamado `mango`

```bash
cd /etc/init.d
sudo nano mango
```

Agrega este código dentro del archivo

```bash
#!/bin/sh
# chkconfig: 235 99 10
# description: Start or stop the Mango Automation
#
### BEGIN INIT INFO
# Provides: mango
# Required-Start: $network $syslog
# Required-Stop: $network
# Default-Start: 2 3 5
# Default-Stop: 0 1 6
# Description: Start or stop the Mango Automation
### END INIT INFO
start="/opt/mango/bin/ma.sh start"
stop="/opt/mango/bin/ma.sh stop"
lockfile=/var/lock/subsys/mango
name='Mango'
case "$1" in
'start')
$start>/dev/null 2>&1 </dev/null
RETVAL=$?
if [ "$RETVAL" = "0" ]; then
touch $lockfile>/dev/null 2>&1
fi
;;
'stop')
$stop
RETVAL=$?
if [ "$RETVAL" = "0" ]; then
rm -f $lockfile
fi
;;
'status')
status Mango
;;
'restart'|'reload'|'condrestart')
$stop ; $start
RETVAL=$?
;;
*)
echo "Usage: sudo service{ start | stop | restart | reload | condrestart}"
RETVAL=1
;;
esac
exit $RETVAL
```

Tenga en cuenta que si tiene un mango instalado en un lugar que no sea: `/opt/mango`, tendrá que cambiar las variables de inicio y finalización en su ruta completa en la que está instalado mango.

Presiona `ctrl+x` para guardar el archivo. Luego, necesitamos colocar los permisos para que el servicio sea ejecutable 

```bash
sudo chmod u+x /etc/init.d/mango
```
Luego tenemos que decirle al sistema sobre esto en sistemas basados en Debian (Ubuntu, Linux Mint, etc.)

```bash
sudo update-rc.d  mango defaults
```

¡Listo! Ahora puede iniciar y detener los servicios de esta manera:

#### Detener
```bash
sudo service mango stop
```

#### Iniciar
```bash
sudo service mango start
```

#### Reiniciar
```bash
sudo service mango restart
```

#### Estatus
```bash
sudo service mango status
```

### Windows Service
Recomendamos usar **YAJSW** para instalar un servicio de Windows. Para esto, proporcionamos un archivo preempaquetado:

1. Descargue el archivo zip yajsw.zip y descomprima el archivo en su carpeta **MA_HOME**.
2. Dentro del archivo `MA_HOME/yajsw/conf/wrapper.conf` puede editar la ubicación donde está instalado Mango. Por defecto asume que Mango está instalado en `C:\Mango`.
3. En el símbolo del sistema navegue a `MA_HOME\yajsw\bat` y ejecute `installService.bat`.
4. Después de instalar el servicio, puede abrir la pantalla de servicios de Windows y comenzar o detener el servicio llamado Mango.

!> **Note**: Cuando se ejecuta como un servicio de Windows de esta manera, la actualización automática del núcleo de Mango no se completará completamente y deberá finalizar la actualización manualmente eliminando la carpeta **MA_HOME\lib** y descomprimiendo el archivo **m2m2-core.xxxzip** que debe ser descargado durante el proceso de actualización.

## Acerca de Java
Te recomendamos que uses Oracle Java. Puedes descargar el JDK apropiado de Oracle en este [enlace](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).

1. Selecciona **JDK Download**.
2. Lee y acepta el Acuerdo de licencia de Oracle para obtener la última versión del **kit de desarrollo Java SE**.
4. Selecciona el archivo apropiado para el sistema operativo en el que deseas instalar Mango.

En algunos sistemas, es posible que también se deba establecer una variable de entorno **JAVA_HOME** que apunte a su directorio de instalación de JDK. Esto está cubierto por el procedimiento de instalación para cada sistema operativo.

## Inicio de Sesión Automático

**Mango** se instala por defecto con el un usuario administrador. Después de iniciar sesión la contraseña debería ser cambiada.

* Nombre de usuario: **admin**
* Contraseña: **admin**