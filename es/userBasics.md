# Descripción general de la interfaz de usuario de Mango Automation

En el siguiente video obtendrá una descripción general de la nueva interfaz de usuario y una introducción a muchas de las nuevas funciones y herramientas.

[![Mango Overview](../img/mango-automation-overview-video.png)](https://vimeo.com/215444420)