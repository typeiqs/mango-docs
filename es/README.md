## Mango Automation

**Mango** es una aplicación de software multiplataforma basada en web que permite a los usuarios acceder y controlar sensores electrónicos, PLC, controladores, bases de datos o servicios web, a través de múltiples protocolos simultáneamente. Mango proporciona una interfaz con la que se pueden crear y configurar diversas fuentes de datos (data sources), a la vez que proporciona una gestión de accesos a usuarios, registro de datos, alarmas y automatización.

Las poderosas herramientas de visualización dan vida a los datos, con tableros (dashboards) dinámicos que proporcionan la capacidad de analizar y agregar datos de una mejor forma. Ésto permite que los usuarios tomen decisiones más acertadas. Mango Automation incluye todo lo necesario para construir sistemas potentes, escalables y modernos.

## Características
- Protocolos integrados.
- Alto rendimiento en almacenamiento de datos.
- Analítica Integrada.
- Informes y facturación.
- Horarios avanzados con calendario de excepciones.
- Escalabilidad del Sistema.
- Visualización de Datos con tecnología de vanguardia.
- Automatización y alarmas.
- IIoT y monitoreo de sitios remotos.

## Comunidad
Los usuarios y el equipo de desarrollo se encuentran en el [Foro](https://forum.infiniteautomation.com/).