
![logo](../_media/logoMango.svg)

> Aplicación de software multiplataforma basada en web. 

* Utiliza el HTML estándar e incluye un poderoso conjunto de herramientas para crear impresionantes vistas, aplicaciones web y pantallas HMI.

[Iniciar](#mango-automation)
[Website](https://infiniteautomation.com)